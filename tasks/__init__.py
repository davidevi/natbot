import os
from invoke import task

# == Paths =====================================================================
SRC = "src"

# == Commands ==================================================================
CMD_DOWNLOAD_SPACY_MODELS = "pipenv run python -m spacy download en_core_web_sm"
CMD_RUN = "pipenv run python -m ddv.natbot"
CMD_LINT = "pipenv run flake8"
CMD_TEST = "pipenv run pytest"


# == Dev Tasks =================================================================
@task
def models(context):
    with context.cd(SRC):
        context.run(f"{CMD_DOWNLOAD_SPACY_MODELS}")


@task
def shell(context):
    with context.cd(SRC):
        context.run(f"{CMD_RUN} --adapter shell")


@task
def web(context):
    with context.cd(SRC):
        context.run(f"{CMD_RUN} --adapter web")


@task
def lint(context):
    with context.cd(SRC):
        context.run(f"{CMD_LINT}")


@task
def test(context):
    with context.cd(SRC):
        os.environ["PYTHONPATH"] = os.getcwd() + "/" + SRC
        context.run(f"{CMD_TEST}")
