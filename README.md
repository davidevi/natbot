# NatBot

Natural Language Bot, a Python-based bot that can be extended using
natural language processing.

The bot makes use of [spaCy](https://spacy.io/) to automatically extract the
relevant natural language information which can then be used as part of plug-ins.

This allows the bot and its extensions to function using semantics rather than
rely on hard-coded values and switch statements.

# Requirements
The following must be present on the system:
- `python 3.7.3+`
- `pip3`
- `pipenv`

# Usage
Install the required dependencies:
```
pipenv install
```
Run the bot with the default adapter using `invoke`:
```
pipenv run invoke run
```
