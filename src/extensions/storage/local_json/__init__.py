"""
    Local JSON storage extension
"""
import os
import json
import logging
from ddv.natbot.extensions.storage import StorageProvider

logger = logging.getLogger(__name__)

# TODO: Add setting for in-memory storage rather
# than repeated reading/writing to disk

# TODO: Add mechanism to allow StorageProvider to
# access NatBot settings


class LocalJSON(StorageProvider):
    def __init__(self, parent_adapter):
        super().__init__(parent_adapter)
        logger.debug("Initialising LocalJSON Storage Provider")

        self.STORAGE_PATH = "./natbot_storage.json"
        self.data = {}

        # If storage file exists, load from it
        if os.path.exists(self.STORAGE_PATH):
            self._read_from_disk()
        # If storage file does not exist, create it
        else:
            self._write_to_disk()

    def __getitem__(self, item):
        super(LocalJSON, self).__getitem__(item)
        return self.data[item]

    def __setitem__(self, item, value):
        super(LocalJSON, self).__setitem__(item, value)
        self.data[item] = value
        self._write_to_disk()

    def _write_to_disk(self):
        with open(self.STORAGE_PATH, 'w') as data_file:
            data_file.write(json.dumps(self.data))

    def _read_from_disk(self):
        with open(self.STORAGE_PATH, 'r') as data_file:
            self.data = json.load(data_file)


STORAGE_CLASS = LocalJSON
