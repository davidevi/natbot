"""
    Timer ability - Allows bot to set timers
"""
from datetime import datetime
import logging
from ddv.natbot.nlp import (get_first_pos, find_verb_in_doc)

logger = logging.getLogger(__name__)

NOUN = ['timer', 'timers']

timers = []


def handler(doc, adapter):

    _clear_elapsed_timers(adapter)

    # User must have asked for a timer to be "set" in order for timer to
    # be enabled
    display_timers = find_verb_in_doc(doc, 'show') is not None
    timer_requested = find_verb_in_doc(doc, 'set') is not None
    timer_cancelation_requested = find_verb_in_doc(doc, 'cancel') is not None

    if display_timers:
        return _display_timers()

    if timer_requested:
        return _set_timer(doc, adapter)

    if timer_cancelation_requested:
        return _cancel_timer(doc, adapter)


def _clear_elapsed_timers(adapter):
    now = datetime.now()

    for timer in timers:
        if timer['send_at'] < now:
            timers.remove(timer)


def _display_timers():

    if len(timers) <= 0:
        return "No timers have been set"

    to_return = "The following timers have been set:\n"
    for timer in timers:
        to_return += f"{timer['value']} {timer['time_unit']}\n"

    return to_return


def _set_timer(doc, adapter):
    (value, time_unit) = _get_timer_duration(doc)
    notification_delay_seconds = _raw_time_to_seconds(value, time_unit)

    notification_id = adapter.notification_system.create_notification(
        sender="Timer",
        message=f"{value} {time_unit} timer has elapsed",
        seconds_delay=notification_delay_seconds)

    send_at = adapter.notification_system.get_notification(
        notification_id)['send_at']

    timers.append({
        "id": notification_id,
        "value": value,
        "time_unit": time_unit,
        "send_at": send_at
    })

    return f"{value} {time_unit} timer has been set"


def _cancel_timer(doc, adapter):

    (value, time_unit) = _get_timer_duration(doc)

    for set_timer in timers:
        if set_timer['value'] == value and set_timer['time_unit'] == time_unit:
            adapter.notification_system.cancel_notification(set_timer['id'])
            timers.remove(set_timer)
            return f"The {value} {time_unit} timer has been canceled"


def _raw_time_to_seconds(value, time_unit):
    delay = value

    # minutes
    if "m" in time_unit:
        delay *= 60
    # hours
    elif "h" in time_unit:
        delay *= 60 * 60

    return delay


def _get_timer_duration(doc):

    time_unit = ""
    time_value = 0

    numeral = get_first_pos(doc, "NUM")
    time_value = int(numeral.text)

    # Assume
    time_unit = "second"
    parent_text = numeral.head.text
    if "s" in parent_text or "m" in parent_text or "h" in parent_text:
        time_unit = numeral.head.text

    logger.debug(f"Timer duration: {time_value} {time_unit}")

    return (time_value, time_unit)


HANDLER = handler
