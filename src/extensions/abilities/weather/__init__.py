"""
    Weather ability - Allows bot to provide weather information and warnings
"""
from ddv.natbot.nlp import is_question

VERB = ['rain']
NOUN = ['weather']


def handler(doc, adapter):

    if "location" not in adapter.storage:
        return "Location not set"


def assess_triggered(doc):
    """
        Determine wether the user did in fact ask for weather information
    """

    return is_question(doc)


HANDLER = handler
