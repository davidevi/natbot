import spacy

from .__init__ import assess_triggered


def test_assess_triggered_remind_me():
    nlp = spacy.load("en_core_web_sm")
    user_messages = ["remind me to", ", remind me to"]

    for msg in user_messages:
        doc = nlp(msg)
        assert assess_triggered(doc)
