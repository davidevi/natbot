"""
    'Reminder' ability

    Allows users to ask the bot to remind them of things
"""
from ddv.natbot.nlp import (find_verb_in_doc)
import logging

VERB = ['remind']
NOUN = ['reminder']

logger = logging.getLogger(__name__)


def handler(doc, adapter):

    if assess_triggered(doc):
        return "OK"

    return "No reminder"


def assess_triggered(doc):
    """
        Determine wether the user does actually intend
        to set a reminder
    """

    # "Remind me ..." format
    remind_verb = find_verb_in_doc(doc, 'remind')
    if remind_verb is not None:
        for child in remind_verb.children:
            if child.pos_ == "PRON" and child.text == "me":
                return True

    # "Set a reminder..." former
    set_verb = find_verb_in_doc(doc, 'set')
    if set_verb is not None:
        for child in set_verb.children:
            if child.pos_ == "NOUN" and child.text == "reminder":
                return True

    return False


HANDLER = handler
