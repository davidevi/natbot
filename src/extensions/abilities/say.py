"""
    'Say' extension

    Gets the bot to say something
"""
from ddv.natbot.nlp import (find_verb_in_doc, get_matching_quote_index)

VERB = ['say']


def handler(doc, adapter):
    # Finding the verb "say"
    token = find_verb_in_doc(doc=doc, verb='say', tense=None)

    if token is not None:
        # If the user uses quotes, return what's between quotes
        if doc[token.i + 1].pos_ == 'PUNCT':
            start_index = token.i + 1
            end_index = get_matching_quote_index(doc, start_index)

            # +1 and -1 in order to not include the quotation marks in
            # the response
            return str(doc[start_index + 1:end_index])

    return 'No idea what to say'


HANDLER = handler
