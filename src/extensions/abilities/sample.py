"""
    Sample extension used to test various extension-related features
"""

# == TRIGGER CONDITIONS =======================================================

# Trigger when verb is mentioned
VERB = ['']

# Trigger when noun is mentioned
NOUN = ['']

# == HANDLERS =================================================================


# Default handler method, is invoked by adapter when the trigger conditions
# above are met
def handler(doc):
    response = ""

    # ...

    return response


# Default conversation handler, used to interact with bot for more than
# just a single sentence
def conversation_handler(doc):
    response = ""
    conversation_ended = False

    # ...

    return (response, conversation_ended)


# == CONFIGURATION ITEMS  =====================================================
HANDLER = handler
