"""
    Time extension.

    Bot responds with the current time.
"""
from datetime import datetime
NOUN = ['time']


def handler(doc):

    # Checking that the user does indeed want to know what the time is
    # TODO

    return datetime.now().strftime("%b %d %Y %H:%M:%S")


HANDLER = handler
