"""
    Commands used to administer the bot
"""
import logging

logger = logging.getLogger(__name__)


def exit(message, adapter):
    """
        Exit command. Exits program
    """
    logger.info("Running command: exit")
    adapter.stop()


def reload(message, adapter):
    adapter.load_extensions()


def clear(message, adapter):
    import os
    os.system('clear')


# Maps command strings to methods
COMMANDS = {
    "exit": exit,
    "reload": reload,
    "rel": reload,
    "clear": clear,
    "cls": clear
}
