"""
    Commands related to natural language processing
"""
import logging
import spacy

logger = logging.getLogger(__name__)


def analyze(message, adapter):
    """
        Analyze command, prints out sentence analysis
    """
    to_return = "Here is the analysis: \n"

    logger.info("Running command: analyze")

    nlp = spacy.load("en_core_web_sm")
    logger.debug(f"Analyzing text: '{message}'")
    doc = nlp(message)

    def print_tree(token, depth=0):
        result = ""
        spaces = ""
        for i in range(0, depth):
            spaces += "  "
        result = spaces + f"({token.i}) - {token.dep_} - {token.text}  [{token.pos_}]\n"
        for child in token.children:
            result += print_tree(child, depth + 1)
        return result

    for token in doc:
        if token.dep_ == "ROOT":
            to_return += print_tree(token)

    return to_return


# Maps command strings to methods
COMMANDS = {"an": analyze}
