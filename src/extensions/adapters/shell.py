"""
    Shell adapter. Allows user to interact with the bot from the command line.
"""
import logging

import sys
from time import sleep
from threading import Thread

from ddv.natbot.extensions.adapters import GenericAdapter

logger = logging.getLogger(__name__)


class ShellAdapter(GenericAdapter):
    def __init__(self, storage_solution):
        super().__init__(storage_solution)
        logger.debug("Shell adapter is being initialised")
        self.INPUT_PROMPT = '> '

    def run(self):
        """
            Main loop for interacting with the user. Prompt user for a message
            and bot responds
        """
        super(ShellAdapter, self).run()
        logger.debug("Shell adapter is starting")

        # Starting notification handler in a separate thread
        self.notification_thread = Thread(target=self.print_notifications)
        self.notification_thread.start()

        # Main loop
        while self.running:
            user_input = self.get_user_input()
            responses = self.handle_message(user_input)
            for response in responses:
                self.respond(response)

        logger.debug("Adapter is no longer running")

    def stop(self):
        super(ShellAdapter, self).stop()

    def print_notifications(self):
        logger.debug("Notification thread is starting")

        while self.running:
            sleep(1)
            notifications = self.notification_system.load_notifications()

            for ntf in notifications:
                print(f"\n- ({ntf['sender']}) {ntf['message']}")
                sys.stdout.write(self.INPUT_PROMPT)
                sys.stdout.flush()

        logger.debug("Notification thread is no longer running")

    def get_user_input(self):
        """
            Prompts the user for a message
        """
        user_input = input(self.INPUT_PROMPT)
        logger.debug(f"User said: '{user_input}'")
        return user_input

    def respond(self, message):
        """
            Responds to a user's message
        """
        logger.debug(f"Responding: '{message}'")
        print(f"- {message}")


ADAPTER = ShellAdapter
