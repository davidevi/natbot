import logging
from flask import Flask, render_template, request, jsonify

server = Flask(__name__)
logger = logging.getLogger(__name__)


@server.route('/', methods=['GET'])
def interface():
    return render_template('index.html')


@server.route('/message', methods=['POST'])
def post_message():
    data = request.get_json()
    message = data["message"]
    logger.debug(f"Message posted: {message}")
    server.adapter.handle_user_input(message)
    return "OK", 200


@server.route('/responses', methods=['GET'])
def get_responses():
    responses = server.adapter.return_responses()
    processed_responses = []
    for r in responses:
        processed_responses.append(str(r))
    logger.debug(f"Returning responses: '{processed_responses}'")
    return jsonify(processed_responses)
