"""
    Web adapter. Allows user to interact with the bot using a web interface.
"""
import logging
from ddv.natbot.extensions.adapters import GenericAdapter
from .server import server

logger = logging.getLogger(__name__)


class WebAdapter(GenericAdapter):
    def __init__(self):
        super().__init__()
        logger.debug("Web adapter is being initialised")
        server.adapter = self
        self.responses = []

    def run(self):
        super(WebAdapter, self).run()
        logger.debug("Web adapter is starting")
        server.run(host='0.0.0.0')

    def handle_user_input(self, message):
        self.responses += self.handle_message(message)

    def return_responses(self):
        to_return = self.responses
        self.responses = []
        return to_return


ADAPTER = WebAdapter
