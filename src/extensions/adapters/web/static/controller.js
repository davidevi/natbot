
$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
            var keycode = (ev.keyCode ? ev.keyCode : ev.which);
            if (keycode == '13') {
                fnc.call(this, ev);
            }
        })
    })
}

$(document).ready(function () {

    $("#user-message-box").focus();

    $("#user-message-box").on('keypress', function(e) {
        if (e.which == 13) {

            user_message = $("#user-message-box").val();

            message_block = "<div class='user-message-block message-block'>" + user_message + "</div>"
            $(".message-area").html($(".message-area").html() + message_block);
            $(".message-area").animate({ scrollTop: $(document).height() }, 1000);

            $.ajax({
                type: "POST",
                url: "/message",
                data: JSON.stringify({
                    "message": user_message
                }),
                contentType : 'application/json',
                success: function() {
                    $("#user-message-box").val("");
                }
            })
        }
    })

    setInterval(function(){
        $.ajax({
            type: "GET",
            url: "/responses",
            success: function(data) {
                console.log("Responses: "+ data);

                for (const response of data) {

                    processed_response = response.replace(/\n/gi, "<br/>");
                    processed_response = processed_response.replace(/  /gi, "&nbsp&nbsp");

                    message_block = "<div class='bot-message-block message-block'>" + processed_response + "</div>"
                    $(".message-area").html($(".message-area").html() + message_block);
                    $(".message-area").animate({ scrollTop: $(document).height() }, 1000);
                }
            }
        })
     }, 3000);


})
