"""
    Module that handles push notifications
"""
from random import randint
from datetime import (datetime, timedelta)


class NotificationSystem(object):
    def __init__(self):
        self.ready_queue = []
        self.delay_queue = []

    def create_notification(self, message, sender=None, seconds_delay=None):

        if seconds_delay:
            # Notification ID is set so that the notification
            # can be canceled if necessary
            notification_id = randint(0, 999)
            send_at = datetime.now() + timedelta(seconds=seconds_delay)

            self.delay_queue.append({
                'id': notification_id,
                'message': message,
                'sender': sender,
                'send_at': send_at
            })

            return notification_id
        else:
            self.ready_queue.append({
                'message': message,
                'sender': sender
            })

            return None

    def get_notification(self, notification_id):
        for notification in self.delay_queue:
            if notification['id'] == notification_id:
                return notification
        raise KeyError(f"Notification {notification_id} not found")

    def cancel_notification(self, notification_id):
        notification = self.get_notification(notification_id)
        self.delay_queue.remove(notification)

    def load_notifications(self):
        self._transfer_ready_messages()
        to_return = self.ready_queue
        self.ready_queue = []
        return to_return

    def _transfer_ready_messages(self):
        """
            Transfers messages from the delay queue into the ready queue
            if the delay has been met
        """
        now = datetime.now()

        for queued_notification in self.delay_queue:
            if queued_notification['send_at'] < now:
                self.ready_queue.append(queued_notification)
                self.delay_queue.remove(queued_notification)

