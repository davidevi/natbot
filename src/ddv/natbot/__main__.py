"""
    Main entry point to NatBot
"""
import logging
import click

from ddv.natbot.extensions.adapters import AdapterManager
from ddv.natbot.extensions.storage import StorageManager
from ddv.natbot.logging import log_to_stdout

logger = logging.getLogger(__name__)


@click.command()
@click.option('--adapter', prompt='Adapter',
              help='Adapter to use')
@click.option('--storage', prompt='StorageSolution',
              help='Storage solution to use', default='local_json')
def main(adapter, storage='local_json'):
    log_to_stdout()

    logger.info("NatBot starting")
    am = AdapterManager()
    sm = StorageManager()

    try:
        adapter_class = am.get_adapter(adapter)
        storage_class = sm.get_storage(storage)
        adapter = adapter_class(storage_class)
        adapter.run()
    except KeyError:
        logger.error(f"Adapter '{adapter}' could not be found")

    logger.info("NatBot exiting")


if __name__ == '__main__':
    main()
