"""
    Provides methods for facilitating Natural Language Processing

    Mainly intended to be used by bot extensions
"""


def find_verb_in_doc(doc, verb, tense=None):
    """
        Given a verb string
        And a spaCy document
        Return first token with that verb
        Return None if no token has that verb
    """
    for token in doc:
        if token.text == verb:
            if tense is None:
                return token
            else:
                raise NotImplementedError("Searching by tense not implemented")

    return None


def get_matching_quote_index(doc, start_index):
    """
        Given the index of a quotation mark
        And a spaCy document
        Return the index of the matching quotation mark in the document
        Raise error if no matching quotation mark is found
    """
    end_index = None

    for token_index in range(start_index + 1, len(doc)):
        if doc[token_index].pos_ == 'PUNCT' and doc[token_index].text == doc[start_index].text:
            end_index = token_index
            break

    if end_index is None:
        raise Exception("No matching quote found")

    return end_index


def get_first_pos(doc, pos):
    """
        Given a document and a part of speech, return first
        occurence of a token with given part of speech

        Raises KeyError otherwise
    """
    for token in doc:
        if token.pos_ == pos:
            return token
    raise KeyError("POS not found in document")


def is_question(doc):
    # It's always a question if there's a question mark
    for token in doc:
        if token.pos_ == 'PUNCT':
            return token.text == '?'

    # Sometimes users might not put the question mark at the end
    question_adverbs = [
        "who", "what", "where", "when"
    ]
    for token in doc:
        if token.text in question_adverbs:
            return True

    return False
