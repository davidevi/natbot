"""
    Used to load and manage application settings
"""
import os


def cast_bool(string_setting):
    """
        Used to cast system environment variables as boolean
    """
    return "t" in string_setting.lower()


DEBUG = cast_bool(os.environ.get("NATBOT_DEBUG", "false"))
COMMAND_PREFIX = '/'
