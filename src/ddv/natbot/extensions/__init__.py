"""
    Contains methods that help with importing extensions for the bot
"""
import os
from importlib import import_module
import logging

logger = logging.getLogger(__name__)


class ExtensionManager(object):
    """
        Superclass meant to provide reusable methods for loading
        and managing NatBot extensions
    """

    def list_available_extensions(self, extensions_path):
        """
            Given a path, returns an array of available modules found at
            that path
        """
        dir_listing = os.listdir(extensions_path)
        module_names = []

        # Removing non-modules
        for listing in dir_listing:
            listing_abs_path = f"{extensions_path}/{listing}"

            if os.path.isfile(listing_abs_path) and listing.endswith(".py"):
                module_names.append(listing[:-3])

            if os.path.isdir(listing_abs_path) and os.path.exists(
                    listing_abs_path + "/__init__.py"):
                module_names.append(listing)

        logger.debug(
            f"The following extensions have been found: {module_names}")

        return module_names

    def import_extensions(self, extensions_path, extensions):
        """
            Given an array of extension names, and a path where the can be found,
            import all modules from the given path
        """
        modules = []

        for extension in extensions:
            modules.append(self.import_extension(extensions_path, extension))

        return modules

    def import_extension(self, extensions_path, extension):
        """
            Given an extension name, return the imported module for that extension
        """
        logger.info(f"Loading extension '{extension}'")
        package_path = extensions_path.replace('/', '.')[2:]
        return import_module(f"{package_path}.{extension}")
