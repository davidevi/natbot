"""
    Contains methods for loading and managing bot abilities
"""
import logging
from ddv.natbot import settings
from ddv.natbot.extensions import ExtensionManager

logger = logging.getLogger(__name__)


class AbilityManager(ExtensionManager):

    ALLOWED_TRIGGERS = ['VERB', 'NOUN']

    def __init__(self, parent_adapter):
        self.parent_adapter = parent_adapter
        self.ABILITY_HANDLERS = {}
        self.load_abilities()

    def load_abilities(self, extensions_path='./extensions/abilities'):
        """
            Used to load abilities at runtime.
            Returns a dict of verb handlers (methods that are meant to be
            executed when a verb is used or mentioned)
        """
        available_extensions = self.list_available_extensions(extensions_path)
        ability_modules = self.import_extensions(extensions_path,
                                                 available_extensions)
        self._create_ability_handlers(ability_modules)

    def has_handler(self, text, pos):
        valid_trigger = pos in self.ABILITY_HANDLERS
        if valid_trigger:
            logger.debug(f"{pos} is a valid trigger POS")
            handler_exists = text in self.ABILITY_HANDLERS[pos]
            if handler_exists:
                logger.debug(f"Handler found for '{text}' ({pos})")
                return True
            logger.debug(f"Handler not found for '{text}' ({pos})")
            logger.debug(self.ABILITY_HANDLERS)
        logger.debug(f"{pos} is not a valid trigger POS")
        return False

    def get_ability_responses(self, token, doc):
        responses = []
        for handler in self.ABILITY_HANDLERS[token.pos_][str(token)]:
            try:
                response = handler(doc, self.parent_adapter)
                if response is not None:
                    logger.debug('Action response:')
                    logger.debug(response)
                    responses.append(response)
                else:
                    logger.debug("No response returned by ability handler")
            except NotImplementedError as e:
                responses.append(e)
            except Exception as e:
                logger.error(f"Error in extension: {e}")
                if settings.DEBUG:
                    import traceback
                    traceback.print_exc()

        return responses

    def _create_handlers_for_trigger_type(self, trigger_type, ability_module):
        """
            Given a trigger name and an ability module, return a handler
            dict

            ^ ?????? ^ TODO: Better description
        """
        handlers = {trigger_type: {}}

        if self._module_has_trigger(trigger_type, ability_module):
            trigger_words = getattr(ability_module, trigger_type)
            for trigger in trigger_words:
                if trigger in handlers[trigger_type]:
                    handlers[trigger_type][trigger].append(
                        ability_module.HANDLER)
                else:
                    handlers[trigger_type][trigger] = [ability_module.handler]
        return handlers

    def _create_ability_handlers(self, ability_modules):
        """
            Given an array of ability modules, return
            a dict of handlers
        """
        for module in ability_modules:
            validation_errors = self._validate_ability_module(module)
            is_valid_ability = len(validation_errors) <= 0

            if not is_valid_ability:
                logger.error(
                    f"Errors have been found with ability '{module.__name__}':"
                )
                for error_message in validation_errors:
                    logger.error(f" - {error_message}")
                continue

            for allowed_trigger_type in self.ALLOWED_TRIGGERS:
                if self._module_has_trigger(allowed_trigger_type, module):
                    trigger_type_handlers = self._create_handlers_for_trigger_type(
                        allowed_trigger_type, module)
                    logger.debug(
                        f"The following handlers have been created for trigger type '{allowed_trigger_type}':"
                    )
                    logger.debug(trigger_type_handlers)

                    for trigger_type in trigger_type_handlers:
                        if trigger_type in self.ABILITY_HANDLERS:
                            self.ABILITY_HANDLERS[trigger_type].update(
                                trigger_type_handlers[trigger_type])
                        else:
                            self.ABILITY_HANDLERS[
                                trigger_type] = trigger_type_handlers[
                                    trigger_type]

        logger.debug(f"Resulting ability handlers: {self.ABILITY_HANDLERS}")

    def _module_has_trigger(self, trigger_name, ability_module):
        """
            Given the name of a trigger and an ability module,
            return True if ability module has an attribute with the name
            of given trigger; false otherwise
        """
        try:
            getattr(ability_module, trigger_name)
            return True
        except AttributeError:
            return False

    def _validate_ability_module(self, ability_module):
        found_errors = []

        # Must have a valid trigger
        valid_trigger_found = False
        for trigger in self.ALLOWED_TRIGGERS:
            valid_trigger_found = self._module_has_trigger(
                trigger, ability_module)
            if valid_trigger_found:
                logger.debug(
                    f"Trigger '{trigger}' has been found for {ability_module.__name__}"
                )
                break
        if not valid_trigger_found:
            found_errors.append("No trigger words have been defined")

        # Trigger of any kind must be a valid word
        # TODO

        # Must have some kind of handler
        try:
            getattr(ability_module, 'HANDLER')
        except AttributeError:
            found_errors.append("No handlers have been defined")

        return found_errors
