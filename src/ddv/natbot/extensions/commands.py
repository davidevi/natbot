import logging
from ddv.natbot import settings
from ddv.natbot.extensions import ExtensionManager

logger = logging.getLogger(__name__)


class CommandManager(ExtensionManager):
    def __init__(self, parent_adapter):
        self.COMMANDS = {}
        self.load_commands()

    def load_commands(self, extensions_path='./extensions/commands'):
        logger.debug("Loading commands")
        available_extensions = self.list_available_extensions(extensions_path)
        command_modules = self.import_extensions(extensions_path,
                                                 available_extensions)

        valid_command_modules = []

        for cmd_module in command_modules:
            if len(self._validate_command_module(cmd_module)) == 0:
                valid_command_modules.append(cmd_module)

        for cmd_module in valid_command_modules:
            self.COMMANDS.update(cmd_module.COMMANDS)

        logger.debug(
            f"The following commands have been loaded: {self.COMMANDS}")

    def _validate_command_module(self, module):
        errors = []

        # TODO

        return errors

    def is_command(self, message):
        """
            Returns true if a user's message is meant to be interpreted
            as a command. False otherwise.
        """
        command_detected = message.startswith(settings.COMMAND_PREFIX)
        if command_detected:
            logger.debug(f"Command detected in {message}")
        return command_detected

    def run_command(self, command_message, adapter):
        """
            Given a command name, runs the command
        """
        # Removing command prefix from command name
        start_cut = command_message.index(settings.COMMAND_PREFIX) + len(
            settings.COMMAND_PREFIX)
        command_message = command_message[start_cut:]

        command_name = command_message[:command_message.index(
            ' ')] if ' ' in command_message else command_message
        logger.debug(f"Command name: '{command_name}'")
        commmand_arguments = command_message[
            command_message.index(' ') +
            1:] if ' ' in command_message else None
        logger.debug(f"Command arguments: '{commmand_arguments}'")

        if command_name in self.COMMANDS:
            logger.debug(f"Running command: {command_name}")
            return self.COMMANDS[command_name](commmand_arguments, adapter)
        else:
            raise KeyError(f"Command not found: {command_name}")
