"""
    An adapter allows users to interact with NatBot through various channels
    such as shell, messaging systems, e-mail or SMS.
"""
import sys
import spacy
import logging

from ddv.natbot.extensions import ExtensionManager
from ddv.natbot.extensions.commands import CommandManager
from ddv.natbot.extensions.abilities import AbilityManager
from ddv.natbot.notifications import NotificationSystem

logger = logging.getLogger(__name__)


class AdapterManager(ExtensionManager):
    def get_adapter(self,
                    adapter_name,
                    extensions_path='./extensions/adapters'):
        """
            Given an adapter name and the path to the adapter extensions
            directory, import the matching adapter module and return an
            initialised adapter class

            Returns KeyError if no adapter matching the name can be found2
        """
        module_names = self.list_available_extensions(extensions_path)

        # If the adapter is not in the list of importable modules, raise error
        if adapter_name not in module_names:
            raise KeyError(f"Adapter '{adapter_name}' could not be found")

        # Importing adapter module
        adapter_module = self.import_extension(extensions_path, adapter_name)
        validation_errors = self._validate_adapter_module(adapter_module)

        # Return initialised adapter if there's no validation errors
        if len(validation_errors) == 0:
            logger.debug(f"Adapter '{adapter_name}' has been found")
            return adapter_module.ADAPTER

        # Print out errors if there is
        logger.error(
            f"Could not use adapter '{adapter_name}' due to the following errors:"
        )
        for error in validation_errors:
            logger.error(error)
        sys.exit(1)

    def _validate_adapter_module(self, module):

        errors = []

        try:
            getattr(module, 'ADAPTER')
        except AttributeError:
            errors.append("No exported ADAPTER could be found")

        return errors


class GenericAdapter(object):
    def __init__(self, storage_manager):
        logger.debug("Adapter is now being initialised")
        self.running = False
        self.nlp = spacy.load("en_core_web_sm")
        self.notification_system = NotificationSystem()
        self.load_extensions(storage_manager)

    def load_extensions(self, storage_manager):
        logger.debug("Loading ability extensions")
        self.ability_manager = AbilityManager(parent_adapter=self)
        logger.debug("Loading command extensions")
        self.command_manager = CommandManager(parent_adapter=self)
        logger.debug("Loading storage extensions")
        self.storage = storage_manager(parent_adapter=self)

    def run(self):
        """
            Starts listening for user commands and responding to them

            Meant to be overwritten by subclasses
        """
        logger.debug("Adapter is now starting")
        self.running = True

    def stop(self):
        logger.debug("Adapter is now stopping")
        self.running = False

    def handle_message(self, user_message):
        """
            Given a message from the user, check if it's a command
            (and run the command if it is one)

            Otherwise, analyse the text, see if any extensions can respond
            to the user's message, and then return responses
        """
        # If it's a command, executing it
        if self.command_manager.is_command(user_message):
            try:
                return [self.command_manager.run_command(user_message, self)]
            except KeyError:
                return [f"Unknown command {user_message}"]

        doc = self.analyze(user_message)
        return self.find_responses(doc)

    def analyze(self, text):
        """
            Runs tokenizer, tagger, parser, NER and word vectors
        """
        logger.debug(f"Analyzing text: {text}")
        doc = self.nlp(text)

        for token in doc:
            logger.debug(
                f"{token.text} {token.dep_} {token.head.text} {token.head.pos_} {[child for child in token.children]}"
            )

        return doc

    def find_responses(self, doc):
        """
            Given an analyzed message (a document), find an appropriate
            response
        """
        logger.debug(f"Finding responses in document '{doc}'")
        responses = []

        for token in doc:
            logger.debug(f"Checking token '{token}' ({token.pos_})")

            # Checking if triggers exist for this POS
            if self.ability_manager.has_handler(token.text, token.pos_):
                logger.debug(
                    f"Found relevant handler for {token.pos_} '{token}'")
                responses += self.ability_manager.get_ability_responses(
                    token, doc)
            else:
                logger.debug(
                    f"No handler has been found for {token.pos_} '{token}'")
                # logger.debug(f"{self.extension_handlers[token.pos_]}")

        return responses
