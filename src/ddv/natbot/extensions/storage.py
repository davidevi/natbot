"""
    Module that handles a bot's permanent storage
"""
import sys
import logging
from ddv.natbot.extensions import ExtensionManager

logger = logging.getLogger(__name__)


class StorageManager(ExtensionManager):
    def __init__(self):
        pass

    def get_storage(self, storage_name,
                    extensions_path='./extensions/storage'):
        module_names = self.list_available_extensions(extensions_path)

        # If the adapter is not in the list of importable modules, raise error
        if storage_name not in module_names:
            raise KeyError(
                f"Storage solution '{storage_name}' could not be found")

        # Importing adapter module
        storage_module = self.import_extension(extensions_path, storage_name)
        validation_errors = self._validate_storage_module(storage_module)

        # Return initialised adapter if there's no validation errors
        if len(validation_errors) == 0:
            logger.debug(f"Storage solution '{storage_module}' has been found")
            return storage_module.STORAGE_CLASS

        # Print out errors if there is
        logger.error(
            f"Could not use storage solution '{storage_module}' due to the following errors:"
        )
        for error in validation_errors:
            logger.error(error)
        sys.exit(1)

    def _validate_storage_module(self, module):

        errors = []

        try:
            getattr(module, 'STORAGE_CLASS')
        except AttributeError:
            errors.append("No exported STORAGE_CLASS could be found")

        return errors


class StorageProvider(object):
    """
        Storage provider superclass, meant to be used by storage extensions
    """

    def __init__(self, parent_adapter):
        logger.debug("Initialising storage provider")
        self.parent_adapter = parent_adapter

    def __getitem__(self):
        pass

    def __setitem__(self):
        pass
