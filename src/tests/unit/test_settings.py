from ddv.natbot.settings import cast_bool


def test_cast_bool_valid():
    assert cast_bool("true")
    assert cast_bool("True")
    assert cast_bool("t")
    assert cast_bool("T")
    assert not cast_bool("false")
    assert not cast_bool("False")
    assert not cast_bool("f")
    assert not cast_bool("F")
