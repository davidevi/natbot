# NatBot Documentation

## Usage


## Project Structure
```
natbot
├── docs
├── src
│   ├── ddv
│   │   └── natbot      --- Core NatBot Sources
│   ├── extensions      --- Sources for NatBot extensions
│   │   ├── abilities   --- Abilities extensions
│   │   ├── adapters    --- Adapters extensions
│   │   ├── commands    --- Commands extensions
│   │   └── storage     --- Storage extensions
│   └── tests           ---
│       └── unit        --- Unit tests for core NatBot
└── tasks               --- Invoke tasks
```
